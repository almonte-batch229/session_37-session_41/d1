const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

// check email if it existing in our database
router.post("/checkemail", (req, res) => {
	userControllers.checkIfEmailExists(req.body).then(resultFromController => {
		res.send(resultFromController);
	});
});

// user registration
router.post("/register", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	});
});

// user login 
router.post("/login", (req, res) => {
	userControllers.loginUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	});
});

// retrieving user details
// auth.verify - middleware to ensure that the user is logged in before they can retrieve their details. 
router.post("/details", auth.verify, (req, res) => {
	// uses the decode method defined in the auth.js to retrieve user info from request header.
	const userData = auth.decode(req.headers.authorization);

	userControllers.getProfile({userId: req.body.id}).then(resultFromController => {
		res.send(resultFromController);
	});
});

// enroll a user
router.post("/enroll", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}

	userControllers.enroll(data).then(resultFromController => {
		res.send(resultFromController);
	});
})

module.exports = router;