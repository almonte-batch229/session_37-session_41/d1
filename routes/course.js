const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

// Create Courses
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		courseControllers.addCourse(req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("User is not authorized to create courses")
	}
});

// Retrieve All Courses
router.get("/all", (req, res) => {
	courseControllers.getAllCourses().then(resultFromController => {
		res.send(resultFromController);
	});
});

// MINI ACTIVITY
// Retrieve All Active Courses
router.get("/active", (req, res) => {
	courseControllers.getAllActiveCourses().then(resultFromController => {
		res.send(resultFromController);
	});
});

// Retrieve a specific Course
router.get("/:courseId", (req, res) => {
	courseControllers.getCourse(req.params).then(resultFromController => {
		res.send(resultFromController);
	});
});

// Update a Course
router.put("/:courseId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		courseControllers.updateCourse(req.params, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("User is not authorized to commit changes")
	}
});

// Archive Course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		courseControllers.archiveCourse(req.params, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("User is not authorized to commit changes")
	}
});

module.exports = router;