const Course = require("../models/Course");

// Create New Course
/*
	1. Create a new Course object using the mongoose model
	2. Save the new Course to the database
*/
module.exports.addCourse = (reqBody) => {
	// create a new variable "newCourse" and instantiate a new Course object
	// uses the info from the current request body to provide all necessary info
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	// saves the created object to our database
	return newCourse.save().then((course, error) => {
		// course creation failed
		if(error) {
			return false;
		}
		// course creation success
		else {
			return true;
		}
	});
};

// Retrieve All Courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// MINI ACTIVITY
// Retrieve All Active Courses
module.exports.getAllActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
			return result;
	});
};

// Retrieve Specific Course
module.exports.getCourse = (reqParams) => {
	// console.log(reqParams)

	return Course.findById(reqParams.courseId).then(result => {
			return result;
	});
};

// Update a Course
/*
	1. Create a variable "updatedCourse" which will contain the info retrieved from the req body
	2. Find and update the course using the course ID retrieved from the req params and the variable "updatedCourse"
*/
// data for updating will be coming from URL parameters and requestbody
module.exports.updateCourse = (reqParams, reqBody) => {
	// specify the fields of the document to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	// perform findByIdAndUpdate by finding the id and update the properties of the document.
	// findByIdAndUpdate(document_id, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		}
	})
};

module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		}
	})
}


