const bcrypt = require("bcrypt");
const User = require("../models/User");
const Course = require("../models/Course");
const auth = require("../auth");

// Check if the email already exists
/* 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/
module.exports.checkIfEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then( result => {
		if(result.length > 0) {
			return true;
		}
		else {
			return false;
		}
	});
};

// User registration
/*
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 is the value provided as the number of "salt"
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		}
	});
};

// User authentication
/*
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then( result => {
		if(result == null) {
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)};
			}
			else {
				return false;
			}
		}
	});
};

// Get Profile
/*
	1. Find the document in the database using the User's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend
*/
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		// console.log(result);

		result.password = "";

		return result;
	});
};

// enroll user
/*
	1. Find the document in the database using the user's id
	2. Add the course id to the user's enrollment array
	3. Update the document
*/
// Async Await -> will be used in enrolling the user
// this will need to update 2 separate documents
module.exports.enroll = async(data) => {
	// create isUserUpdated variable that will return true upon successful update
	// await -> will allow the enroll method to complete updating the user before returning a response back to the front end
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// it will add the courseId to the user's enrollment array
		user.enrollments.push({courseId: data.courseId});

		// saves the updated user information in the database
		return user.save().then((user, error) => {
			// if failed,
			if(error) {
				return false;
			}
			// if successful,
			else {
				return true;
			}
		});
	});

	// create isCourseUpdated variable that will return true upon successful update
	// await -> will allow the enroll method to complete updating the course before returning a response back to the front end
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// it will add the userId to the course's enrollees array
		course.enrollees.push({userId: data.userId});

		// saves the updated course information in the database
		return course.save().then((course, error) => {
			// if failed,
			if(error) {
				return false;
			}
			// if successful,
			else {
				return true;
			}
		});
	});

	// condition which will check if BOTH the user and course document have been updated
	if (isUserUpdated && isCourseUpdated) {
		return true;
	}
	else {
		return false;
	}
};