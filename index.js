// Setup the modules/dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

// Setup the server
const app = express();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Database Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.3mzqerb.mongodb.net/session37-session41?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the coud database"));

// Server Listening
app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.port || 4000}.`));