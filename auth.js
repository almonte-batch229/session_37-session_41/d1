const jwt = require("jsonwebtoken");

// [SECTION] JSON Web Tokens
/*
	- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
	- Information is kept secure through the use of the secret code
	- Only the system that knows the secret code that can decode the encrypted information
	- Imagine JWT as a gift wrapping service that secures the gift with a lock
	- Only the person who knows the secret code can open the lock
	- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
	- This ensures that the data is secure from the sender to the receiver
*/

// Token Creation
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
};

// Token Verification
/*
	Analogy:
		Receive the gift and open it to verify if the sender is legitimate and the gift was not tampered.
*/
module.exports.verify = (req, res, next) => {

	// token is retrieved from the request header
	let token = req.headers.authorization;
	console.log(token);

	// if token received and is not undefined
	if(typeof token !== "undefined") {
		console.log(token);

		// "slice" method -> extract a particular section of a string without modifying the original string.
		// Bearer nv457nhgvh43857tf347r5783784
		// this removes the word Bearer including the space after to get the token value ONLY.
		token = token.slice(7, token.length);

		// validate the token using the verify method decrypting the token using the secret code
		return jwt.verify(token, secret, (err,data) => {
			// if JWT is not valid
			if(err) {
				return res.send({auth: "failed"});
			}
			// if JWT is valid
			else {
				// proceed to the next middleware function/callback in the route
				next();
			}
		})
	}
	// token does not exist
	else {
		return res.send({auth: "failed"});
	}
};

// Token Decryption
/*
	Analogy:
		Open the gift and get the content
*/
module.exports.decode = (token) => {

	// if token received and is not undefined
	if(typeof token !== "undefined") {

		// this removes the word Bearer including the space after to get the token value ONLY.
		token = token.slice(7, token.length);

		// verification
		return jwt.verify(token, secret, (err,data) => {
			// if JWT is not valid
			if(err) {
				return null;
			}
			// if JWT is valid
			else {
				// decode method -> used to obtain info from jwt
				// complete: true -> option that allows to return additional info from the JWT token
				// payload -> contains info provided in the "createAccessToken" (id, email, isAdmin)
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else {
		return null;
	}
};